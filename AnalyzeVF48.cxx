#include "AnalyzeVF48.h"

#include "TGNumberEntry.h"
#include <TGLabel.h>

#include "TRootanaEventLoop.hxx"
 
AnalyzeVF48::AnalyzeVF48(): TCanvasHandleBase("VF48"){

  fUnpVF48 = new UnpackVF48();
  fUnpVF48->SetNumModules(2);
  fUnpVF48->SetModulesMask(0x3); // 2 modules

  int maxsamples = 0;

  for (int i=0; i<2; i++) {
     int v = 0;
     TRootanaEventLoop::Get().GetODB()->RIAI("Equipment/VME/Settings/VF48_NumSamples", i, &v);
     printf("VF48 %d num samples %d\n", i, v);
     fUnpVF48->SetNumSamples(i, v);
     if (v > maxsamples)
        maxsamples = v;
  }

  for(int i = 0; i < kMaxWf; i++){
    char name[100];
    char title[100];
    sprintf(name,"vfw%i",i);
    sprintf(title,"Waveform %d",i);
    fWaveform[i] = new TH1D(name, title, maxsamples, 0, maxsamples);
  }
}

AnalyzeVF48::~AnalyzeVF48() {
   delete fUnpVF48;
   fUnpVF48 = NULL;
}

void AnalyzeVF48::SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display){


  // Now create my embedded canvas, along with the various buttons for this canvas.
  
  TGHorizontalFrame *labelframe = new TGHorizontalFrame(compFrame,200,40);
  
  fBankCounterButton = new TGNumberEntry(labelframe, 0, 9,999, TGNumberFormat::kNESInteger,
					      TGNumberFormat::kNEANonNegative, 
					      TGNumberFormat::kNELLimitMinMax,
					      0, 31);

  fBankCounterButton->Connect("ValueSet(Long_t)", "TRootanaDisplay", display, "UpdatePlotsAction()");
  fBankCounterButton->GetNumberEntry()->Connect("ReturnPressed()", "TRootanaDisplay", display, "UpdatePlotsAction()");
  labelframe->AddFrame(fBankCounterButton, new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5));
  TGLabel *labelMinicrate = new TGLabel(labelframe, "ADC Channel (0-31)");
  labelframe->AddFrame(labelMinicrate, new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5));

  compFrame->AddFrame(labelframe, new TGLayoutHints(kLHintsCenterX,2,2,2,2));


}



/// Reset the histograms for this canvas
void AnalyzeVF48::ResetCanvasHistograms(){

   for(int i = 0; i < kMaxWf; i++)
      fWaveform[i]->Reset();
}

void AnalyzeVF48::DrawWaveform(int iwf, VF48event* eve, int module, int chan)
{
   VF48module *m = eve->modules[module];
   if (m) {
      VF48channel *c = &m->channels[chan];
      int n = c->numSamples;
      if (n > 0) {
         for (int i=0; i<n; i++) {
            int a = c->samples[i];
            fWaveform[iwf]->SetBinContent(i+1, a);
         }
      }
   }
}
  
/// Update the histograms for this canvas.
void AnalyzeVF48::UpdateCanvasHistograms(TDataContainer& dataContainer){

  // FIXME get midas data bank here

   printf("Here!\n");

   int size;
   void *vf48_ptr;

   size = dataContainer.GetMidasData().LocateBank(NULL, "WFA0", &vf48_ptr);
   if (vf48_ptr) { fUnpVF48->UnpackStream(0, vf48_ptr, size);  }   

   size = dataContainer.GetMidasData().LocateBank(NULL, "WFA1", &vf48_ptr);
   if (vf48_ptr) { fUnpVF48->UnpackStream(1, vf48_ptr, size);  }   

   size = dataContainer.GetMidasData().LocateBank(NULL, "WFA2", &vf48_ptr);
   if (vf48_ptr) { fUnpVF48->UnpackStream(2, vf48_ptr, size);  }   

   size = dataContainer.GetMidasData().LocateBank(NULL, "WFA3", &vf48_ptr);
   if (vf48_ptr) { fUnpVF48->UnpackStream(3, vf48_ptr, size);  }   

   VF48event *vf48eve = fUnpVF48->GetEvent();
   if (vf48eve) {
      printf("ZZZ!\n");
      vf48eve->PrintSummary();

      DrawWaveform(0, vf48eve, 0, 0);

      delete vf48eve;
   }
}
  
/// Plot the histograms for this canvas
void AnalyzeVF48::PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas){

  TCanvas* c1 = embedCanvas->GetCanvas();
  c1->Clear();

  int whichbank = fBankCounterButton->GetNumberEntry()->GetIntNumber();

  if (whichbank >= 0 && whichbank < kMaxWf)
    fWaveform[whichbank]->Draw();

  c1->Modified();
  c1->Update();

}
