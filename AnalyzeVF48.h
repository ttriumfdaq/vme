#ifndef AnalyzeVF48_h
#define AnalyzeVF48_h


#include <iostream>
#include <string>

#include "TH1D.h"
#include "TCanvasHandleBase.hxx"
#include "TGNumberEntry.h"

#include "UnpackVF48A.h"

const int kMaxWf = 32;

class AnalyzeVF48 : public TCanvasHandleBase
{
 public:
   AnalyzeVF48();
   ~AnalyzeVF48();

   /// Reset the histograms for this canvas
   void ResetCanvasHistograms();
      
   /// Update the histograms for this canvas.
   void UpdateCanvasHistograms(TDataContainer& dataContainer);
         
   /// Plot the histograms for this canvas
   void PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas);

   void SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display);
  

private:
   UnpackVF48 *fUnpVF48;
   TH1D *fWaveform[kMaxWf];
   TGNumberEntry *fBankCounterButton;
   void DrawWaveform(int iwf, VF48event* eve, int module, int chan);
};


#endif
