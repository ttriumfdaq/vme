#ifndef ProcessVTR_h
#define ProcessVTR_h

#include "TH1D.h"
#include "TGraph.h"
#include "TCanvasHandleBase.hxx"

class AnalyzeVTR : public TCanvasHandleBase
{
public:
   AnalyzeVTR();
   ~AnalyzeVTR();

  /// Reset the histograms for this canvas
  void ResetCanvasHistograms();
  
  /// Update the histograms for this canvas.
  void UpdateCanvasHistograms(TDataContainer& dataContainer);
  
  /// Plot the histograms for this canvas
  void PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas);

  void SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display);

private:
  uint32_t fPrevEventNo;
  uint32_t fPrevTrigTimeClk;

  TH1D *fHtrigLat;
  TH1D *fHreadoutTime;
  TH1D *fHbusyTime;
  TGraph *fGtrigLat;
  TGraph *fGbusyTime;
  TH1D *fHddddTime;

  uint32_t fFirstEventNo;
  double   fMaxTrigLat;
  double   fMaxBusyTime;
};

#endif
